package com.lezione25.awt.hw_2;

public class Main {

	public static void main(String[] args) {

		/**
		 * Creare un sistema di gestione del menu di un ristorante per il Food Delivery.
		 * Il menu ha al suo interno pietanze e bevande.
		 *
		 * Ogni pietanza sar� caratterizzata ALMENO da:
		 * - Nome prodotto
		 * - Prezzo
		 * - Descrizione 
		 * - Codice prodotto (univoco)
		 * 
		 * Creare una interfaccia AWT che permetta la gestione del menu.
		 * ATTENZIONE: Cercare un modo per creare due liste di prodotti (una per le pietanze e l'altra per le bevande) 
		 * in modo che, alla selezione del prodotto, io possa modificarlo!
		 * 
		 * OPZIONALE (ma non troppo): la gestione deve avere ripercussioni sul DB
		 */
		
	}

}
