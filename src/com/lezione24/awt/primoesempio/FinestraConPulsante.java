package com.lezione24.awt.primoesempio;

import java.awt.Button;
import java.awt.Frame;

public class FinestraConPulsante extends Frame {

	FinestraConPulsante() {

		Button pulsante = new Button("CLICCAMI!");
		pulsante.setBounds(5, 30, 200, 80);
		add(pulsante);
		
		setSize(300, 200);
		setLayout(null);
		setVisible(true);
		
	}
	
}
