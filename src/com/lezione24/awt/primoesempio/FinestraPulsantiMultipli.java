package com.lezione24.awt.primoesempio;

import java.awt.Button;
import java.awt.Frame;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

public class FinestraPulsantiMultipli extends Frame implements ActionListener {

	private Button pulsante_1;
	private Button pulsante_2;
	private Button pulsante_3;
	
	FinestraPulsantiMultipli(){

		
		
		//Creazione di un pulsante
		this.pulsante_1 = new Button("Pulsante 1");
		this.pulsante_1.setBounds(50, 120, 100, 50);
		add(this.pulsante_1);
		this.pulsante_1.addActionListener(this);
		
		//Creazione di un pulsante
		this.pulsante_2 = new Button("Pulsante 2");
		this.pulsante_2.setBounds(50, 180, 100, 50);
		add(this.pulsante_2);
		this.pulsante_2.addActionListener(this);
		

		this.pulsante_3 = new Button("Pulsante 3");
		this.pulsante_3.setBounds(50, 240, 100, 50);
		add(this.pulsante_3);
		//Aggiungo un nuovo Action Listerner dedicato al pulsante 3
		this.pulsante_3.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("Sono il pulsante 3");
				
			}
		});
		
		//Setup del frame
		setSize(500, 500);
		setLayout(null);
		setVisible(true);
		
		WindowListener win_l = new WindowListener() {
			
			@Override
			public void windowOpened(WindowEvent e) {
				// TODO Auto-generated method stub
				System.out.println("Apertura della finestra effettuata!");
				
			}
			
			@Override
			public void windowIconified(WindowEvent e) {
				// TODO Auto-generated method stub
				System.out.println("Sono stato ridotto a icona");
			}
			
			@Override
			public void windowDeiconified(WindowEvent e) {
				// TODO Auto-generated method stub
				System.out.println("Sono stato ripristinato");
				
			}
			
			@Override
			public void windowDeactivated(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowClosing(WindowEvent e) {
				// TODO Auto-generated method stub
				System.out.println("Richiesta di chiusura");
				dispose();
			}
			
			@Override
			public void windowClosed(WindowEvent e) {
				// TODO Auto-generated method stub
				System.out.println("Sono stato chiuso");
				
			}
			
			@Override
			public void windowActivated(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
		};
		
		addWindowListener(win_l);
		
	}

	//Due pulsanti che utilizzano lo stesso metodo e ne derivo la sorgente grazie al getSource
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == this.pulsante_1) {
			System.out.println("Sono il pulsante 1");
		}

		if(e.getSource() == this.pulsante_2) {
			System.out.println("Sono il pulsante 2");
		}
		
	}
	
}
