package com.lezione24.awt.primoesempio;

import java.awt.Button;
import java.awt.Frame;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FinestraConPulsanteEInput extends Frame implements ActionListener {
	
	private TextField nome;
	
	FinestraConPulsanteEInput(){
		
		Label lab = new Label("CIAO");
		lab.setBounds(50, 50, 100, 50);
		add(lab);
	
		//Creazione di un pulsante
		Button pulsante = new Button("Dimmi ciao!");
		pulsante.setBounds(50, 120, 100, 50);
		add(pulsante);
		pulsante.addActionListener(this);
		
		this.nome = new TextField();
		this.nome.setBounds(50, 100, 100, 20);
		add(this.nome);
		
		//Setup del frame
		setSize(500, 500);
		setLayout(null);
		setVisible(true);
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
		System.out.println(this.nome.getText());
		
	}
	
}
