package com.lezione24.awt.hw;

import java.awt.Button;
import java.awt.Frame;
import java.awt.Label;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;

public class FinestraInserimento extends Frame implements ActionListener{

	private TextField 	tfNome;
	private TextField 	tfCognome;
	private TextField 	tfMatricola;
	private TextArea 	taElenco;
	private Button 		btInserisci;
	
	private ArrayList<Studente> elenco = new ArrayList<Studente>();
	
	FinestraInserimento(){
		
		Label lblNome = new Label("Nome:");
		lblNome.setBounds(25, 30, 200, 20);
		add(lblNome);
		
		tfNome = new TextField();
		tfNome.setBounds(25, 50, 200, 20);
		add(tfNome);
		
		Label lblCognome = new Label("Cognome:");
		lblCognome.setBounds(25, 80, 200, 20);
		add(lblCognome);
		
		tfCognome = new TextField();
		tfCognome.setBounds(25, 100, 200, 20);
		add(tfCognome);
		
		Label lblMatricola = new Label("Matricola:");
		lblMatricola.setBounds(25, 130, 200, 20);
		add(lblMatricola);
		
		tfMatricola = new TextField();
		tfMatricola.setBounds(25, 150, 200, 20);
		add(tfMatricola);
		
		btInserisci = new Button("Inserisci");
		btInserisci.setBounds(25, 200, 200, 20);
		add(btInserisci);
		
		btInserisci.addActionListener(this);
		
		Label lblElenco = new Label("Elenco:");
		lblElenco.setBounds(250, 30, 200, 20);
		add(lblElenco);
		
		taElenco = new TextArea();
		taElenco.setText("Attendo istruzioni");
		taElenco.setBounds(250, 50, 200, 170);
		add(taElenco);
		
		
		setSize(500, 300);
		setLayout(null);
		setVisible(true);
		
		addWindowListener(new WindowListener() {
			
			@Override
			public void windowOpened(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowIconified(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowDeiconified(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowDeactivated(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowClosing(WindowEvent e) {
				// TODO Auto-generated method stub
				dispose();
				
			}
			
			@Override
			public void windowClosed(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowActivated(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
	}

	private String stampaElenco() {
		String risultato = "";
		
		for(int i=0; i<this.elenco.size(); i++) {
			risultato += this.elenco.get(i).toString() + "\n";
		}
		
		return risultato;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		String varNome = this.tfNome.getText();
		String varCognome = this.tfCognome.getText();
		String varMatricola = this.tfMatricola.getText();
		
		if(varNome.isEmpty() || varCognome.isEmpty() || varMatricola.isEmpty()) {
			//TODO: Caccia una dialog
			System.out.println("Qualcosa non va!");
			return;
		}
		
		Studente stud_temp = new Studente();
		stud_temp.setNome(varNome);
		stud_temp.setCognome(varCognome);
		stud_temp.setMatricola(varMatricola);
		elenco.add(stud_temp);
		
		System.out.println("Inserito correttamente");
		this.taElenco.setText(this.stampaElenco());
		
		this.tfNome.setText("");
		this.tfCognome.setText("");
		this.tfMatricola.setText("");
		
	}
	
}
