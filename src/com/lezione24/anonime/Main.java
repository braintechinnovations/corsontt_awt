package com.lezione24.anonime;

import java.util.ArrayList;

public class Main {

//	public static String dimmiIlNome() {
//		return "Giovanni";
//	}
	
	public static void main(String[] args) {

//		System.out.println(dimmiIlNome());
		
		Runnable run_1 = new Runnable() {
			@Override
			public void run() {
				System.out.println("Sono un runnable");
			}
		};
		
		run_1.run();
		
		Runnable run_alternativo = () -> 
		{
			System.out.println("Sono un runnable alternativo");
			System.out.println("Sono un runnable alternativo 2");
		};
		run_alternativo.run();
		
		
	}

}
