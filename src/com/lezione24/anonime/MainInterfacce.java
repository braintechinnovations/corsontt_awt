package com.lezione24.anonime;

interface Saluta{
	public String dimmiCiao();
	public String salutaGenerico(String nome);
}

@FunctionalInterface
interface Programmiamo{										//Nuova funzione "unica" il cui comportamento viene personalizzato ogni volta!
	public String linguaggioPreferito(String linguaggio);
}

interface Sommatore{
	public int somma(int a, int b);
}

public class MainInterfacce {

	public static void main(String[] args) {

//		Saluta saluta_raffaele = new Saluta() {
//			
//			@Override
//			public String dimmiCiao() {
//				return "Ciao Giovanni";
//			}
//		};
//		
//		System.out.println(saluta_raffaele.dimmiCiao()); 
//		
//		Saluta saluta_andonio = new Saluta() {
//			
//			@Override
//			public String dimmiCiao() {
//				return "Ciao Andonio";
//			}
//		};
//		
//		System.out.println(saluta_andonio.dimmiCiao());
//		
//		Saluta saluta_ioan = () -> {
//			return "Ciao Ioan";
//		};
//		System.out.println(saluta_ioan.dimmiCiao());
//		
//		Saluta saluta_irene = new Saluta() {
//			
//			@Override
//			public String salutaGenerico(String nome) {
//				return nome;
//			}
//			
//			@Override
//			public String dimmiCiao() {
//				// TODO Auto-generated method stub
//				return "Ciao Irene";
//			}
//			
//		};
//		
//		System.out.println(saluta_irene.salutaGenerico("Irene"));
//		
//		/////////////////////////////////////////////////////////////
//		
//		Programmiamo prog_1 = new Programmiamo() {
//			
//			@Override
//			public String linguaggioPreferito(String linguaggio) {
//				return "Ti piace il " + linguaggio;
//			}
//		};
//		
//		Programmiamo prog = (linguaggio) -> {
//			return "Ti piace il " + linguaggio;
//		};
//		
//		System.out.println(prog.linguaggioPreferito("Java"));		
		
//		/////////////////////////////////////////////////////////////

		Sommatore sum = (numero_1, numero_2) -> {
			return (numero_1 + numero_2) * 100;
		};
		
		System.out.println(sum.somma(5, 7));
		
	}

}
